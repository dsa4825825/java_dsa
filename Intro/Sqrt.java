import java.util.Scanner;
class Sqrt{
	static int sqroot(int num){
		int root = -1;
		int high,low,mid;
		high = num;
		low = 1;
		while(low<high){
			mid = (low+high)/2;
			int sqr = mid*mid;
                        if(sqr==num||(sqr<num&&((mid+1)*(mid+1)>num))){
				root = mid;
				break;
			}
		/*	else if(){
				root = mid;
                                break;
			}*/
			else if(sqr<num){
				low = mid+1;
			}
			else if(sqr>num){
				high = mid-1;
			}
		}
		return root;
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number : ");
		int num = sc.nextInt();
		int root = sqroot(num);
		System.out.println("Square root of "+num+" = "+root);
	}

}
