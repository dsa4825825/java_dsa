import java.util.Scanner;
class RotateArr{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array : ");
		int n = sc.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter elements : ");
		for(int i=0;i<n;i++){
			arr[i] = sc.nextInt();
		}
		for(int element : arr){
			System.out.print(element+" ");
		}
		System.out.print("\nEnter no. of rotations towards right : ");
		int rno = sc.nextInt();
		arr = rotate(arr,rno);
		System.out.println();
		for(int element : arr){
			System.out.print(element+" ");
		}
	}
	static int[] rotate(int[] arr,int B){
		int temp,revInd;
		int n = arr.length;
		int k = B%n;
		for(int i=0;i<n/2;i++){
			revInd = n-i-1;

			//swaping
			
		{	temp = arr[i];
			arr[i] = arr[revInd];
			arr[revInd] = temp;
		}

		}
		for(int i=0;i<k/2;i++){
			revInd = k-i-1;

                        //swaping

			temp = arr[i];
                        arr[i] = arr[revInd];
                        arr[revInd] = temp;
		}
		int itr=1;
		for(int i=k;i<(k+n)/2;i++){
			revInd = n-itr;
	
                        //swaping

			temp = arr[i];
                        arr[i] = arr[revInd];
                        arr[revInd] = temp;
			itr++;
		}	
		return arr;
	}
}
